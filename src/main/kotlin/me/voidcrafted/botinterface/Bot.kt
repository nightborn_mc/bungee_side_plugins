package me.voidcrafted.botinterface

import com.auth0.jwt.JWT
import com.auth0.jwt.algorithms.Algorithm
import khttp.responses.Response
import org.json.JSONObject
import java.util.*

class Bot {
    private val auth = "test"
    private val url = "http://sunset:1038/"
    private fun makeRequest(req_url: String, json : JSONObject) : Response {
        json.put("auth", auth)
        return khttp.post(url + req_url, json = json)
    }

    fun registerToken(code: String, uuid: UUID) {
        val algorithm = Algorithm.HMAC256("test")
        val token = JWT.create()
                .withIssuer("me.voidcrafted.bungeeplugins.DiscordAuthenticator")
                .withClaim("minecraft_uuid", uuid.toString())
                .withClaim("nonce", Date().time)
                .withClaim("id", code)
                .sign(algorithm)
        makeRequest("api/register_code", JSONObject(mapOf(
                "code" to token
        )))
    }

}