package me.voidcrafted.bungeeplugins

import net.md_5.bungee.api.plugin.Plugin
import me.voidcrafted.bungeeconfigurator.Configurator

class BungeeSidePlugins : Plugin() {
    lateinit var configurator: Configurator
    override fun onEnable() {
        // You should not put an enable message in your plugin.
        // BungeeCord already does so

        configurator = Configurator(this)
        val joinEventListener = JoinEventListener(this)
        val chatHandler = ChatHandler(this)
        val hubCommand = HubCommand(this)
        val msgCommand = MessageCommand(this)
        val nickCommand = NicknameCommand(this)
        val prefixCommand = CustomPrefixCommand(this)
        val discordAuthenticator = DiscordAuthenticator(this)
        this.proxy.pluginManager.registerListener(this, joinEventListener)
        this.proxy.pluginManager.registerListener(this, chatHandler)
        this.proxy.pluginManager.registerListener(this, discordAuthenticator)
        this.proxy.pluginManager.registerCommand(this, hubCommand)
        this.proxy.pluginManager.registerCommand(this, msgCommand)
        this.proxy.pluginManager.registerCommand(this, prefixCommand)
        this.proxy.pluginManager.registerCommand(this, nickCommand)

    }
}