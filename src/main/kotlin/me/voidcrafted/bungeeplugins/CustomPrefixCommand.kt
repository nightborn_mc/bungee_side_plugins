package me.voidcrafted.bungeeplugins

import kotlinx.coroutines.experimental.launch
import me.lucko.luckperms.LuckPerms
import net.md_5.bungee.api.ChatColor
import net.md_5.bungee.api.CommandSender
import net.md_5.bungee.api.connection.ProxiedPlayer
import net.md_5.bungee.api.plugin.Command
import me.voidcrafted.bungeestrings.MessageGetter
import net.md_5.bungee.chat.ComponentSerializer
import org.json.JSONArray
import org.json.JSONObject


/**
 * Custom prefix commands
 * @permission void.customprefix Allows a custom prefix
 */
class CustomPrefixCommand(val plugin: BungeeSidePlugins) : Command("customprefix", "void.customprefix") {
    override fun execute(sender: CommandSender?, args: Array<out String>?) {

        if (sender !is ProxiedPlayer) {
            sender?.sendMessage(MessageGetter.CustomPrefixCommand.mustBePlayer)
            return
        }
        val p = sender
        args ?: run {
            sender.sendMessage(*MessageGetter.CustomPrefixCommand.mustHaveArgs)
            return
        }

        val desiredPrefix = args.joinToString(" ")
        if (args.isEmpty()) {
            launch {
                val api = LuckPerms.getApi()
                val u = api.getUser(p.uniqueId) ?: return@launch
                u.clearMatching { it.isMeta && it.permission.startsWith("meta.custom_prefix") }
                api.userManager.saveUser(u)
                sender.sendMessage(*MessageGetter.CustomPrefixCommand.prefixRemoveSuccessful)
            }
            return
        }
        val minusColorCodes = desiredPrefix
                .replace(Regex(
                        "&[a-f1-9]"
                ), "")
        println(desiredPrefix + minusColorCodes)
        if (minusColorCodes.length > 16) sender.sendMessage(*MessageGetter.CustomPrefixCommand.greaterThan16)

        launch {
            val api = LuckPerms.getApi()
            val u = api.getUser(p.uniqueId) ?: return@launch
            u.clearMatching { it.isMeta && it.permission.startsWith("meta.custom_prefix") }
            u.setPermission(api.nodeFactory.makeMetaNode("custom_prefix", desiredPrefix).build())
            api.userManager.saveUser(u)
            p.sendMessage(*MessageGetter.CustomPrefixCommand.prefixSetSuccessful(desiredPrefix))
        }
    }
}