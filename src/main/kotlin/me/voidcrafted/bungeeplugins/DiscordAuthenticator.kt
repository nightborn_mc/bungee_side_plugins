package me.voidcrafted.bungeeplugins

import com.auth0.jwt.JWT
import com.auth0.jwt.algorithms.Algorithm
import kotlinx.coroutines.experimental.launch
import me.lucko.luckperms.LuckPerms
import me.lucko.luckperms.api.Node
import me.lucko.luckperms.api.User
import me.voidcrafted.botinterface.Bot
import me.voidcrafted.bungeestrings.MessageGetter
import net.md_5.bungee.api.chat.TextComponent
import net.md_5.bungee.api.event.PostLoginEvent
import net.md_5.bungee.event.EventHandler
import net.md_5.bungee.api.plugin.Listener
import net.md_5.bungee.chat.ComponentSerializer
import org.json.JSONArray
import org.json.JSONObject
import java.util.*

class DiscordAuthenticator(val plugin: BungeeSidePlugins): Listener {
    private fun checkAuth(uuid: UUID): PlayerWithData? {
        val resp = khttp.get("http://sunset:1038/api/get_player/$uuid").jsonObject
        if(resp.getBoolean("error")) return null
        val player = resp.getJSONObject("player")
        return PlayerWithData(player.getString("discord_id"), player.getString("rank"), uuid, player.getString("tag"))
    }

    @EventHandler
    fun onPostLoginEvent(e: PostLoginEvent) {
        launch {
            val auth = checkAuth(e.player.uniqueId) ?: run {
                val id = RandomWordProvider().getRandomWord().replace("\n", "").replace("\r", "").replace(" ", "")
                Bot().registerToken(id, e.player.uniqueId)
                e.player.disconnect(*MessageGetter.DiscordAuthenticator.disconnectMessage(id))
                return@launch
            }
            val api = LuckPerms.getApi()
            val groups = arrayOf( // Staff roles
                    "mafia_don", "tech", "mcadmin", "capo", "evtorg"
            )
            val u: User = api.getUser(e.player.uniqueId) ?: return@launch

            u.permissions.forEach {
                // Remove all group nodes that are admin groups
                if (
                        it.isGroupNode &&  // If it's a group and in the list above
                        groups.map { "group.$it" }.contains(it.permission)
                ) u.unsetPermission(it) // Fucking nuke it
            }
            u.setPermission(
                    api // set the right group
                            .nodeFactory
                            .newBuilder("group.${auth.rank}")
                            .build()
            )
            // Save them back
            api.userManager.saveUser(u)
            PlayerDataStorage.players += auth
        }
    }
}