package me.voidcrafted.bungeeplugins

import java.util.*

object PlayerDataStorage {
    var players: Array<PlayerWithData> = arrayOf()

    fun getPlayer(uuid: UUID): PlayerWithData? {
        players.forEach {
            if (it.uuid == uuid) return it
        }
        return null
    }
}