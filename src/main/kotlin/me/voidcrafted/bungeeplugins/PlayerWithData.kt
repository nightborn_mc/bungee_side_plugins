package me.voidcrafted.bungeeplugins
import java.util.UUID
class PlayerWithData(var discord_id: String, var rank: String, var uuid: UUID, var tag: String)