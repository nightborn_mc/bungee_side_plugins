package me.voidcrafted.bungeeplugins

import net.md_5.bungee.api.connection.ProxiedPlayer
import net.md_5.bungee.api.event.ChatEvent
import net.md_5.bungee.api.plugin.Listener
import net.md_5.bungee.chat.ComponentSerializer
import net.md_5.bungee.event.EventHandler

class ChatHandler(plugin: BungeeSidePlugins) : Listener {
    val plugin = plugin

    @EventHandler
    fun onPlayerMessage(e: ChatEvent) {
        if (e.isCommand) return; else e.isCancelled = true
        val comps = ComponentSerializer.parse(
                ChatFormatter(e.sender as ProxiedPlayer, plugin)
                        .format(e.message)
                        .toString(0)
        )
        plugin.proxy.players.forEach { it.sendMessage(*comps) }

    }

}