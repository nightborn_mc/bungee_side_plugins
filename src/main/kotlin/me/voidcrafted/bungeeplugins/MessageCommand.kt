package me.voidcrafted.bungeeplugins

import me.lucko.luckperms.LuckPerms
import net.md_5.bungee.api.ChatColor
import net.md_5.bungee.api.CommandSender
import net.md_5.bungee.api.chat.TextComponent
import net.md_5.bungee.api.connection.ProxiedPlayer
import net.md_5.bungee.api.plugin.Command
import net.md_5.bungee.chat.ComponentSerializer
import org.json.JSONArray
import org.json.JSONObject

class MessageCommand(val plugin: BungeeSidePlugins) : Command("msg", "bungeecord.message", "message", "whisper", "tell", "m") {
    override fun execute(sender: CommandSender?, args: Array<out String>?) {
        var s = (sender as ProxiedPlayer)
        if (args == null || args.size < 2) return s.sendMessage(TextComponent(ChatColor.RED.toString() + "Incorrect usage - /msg <player> <message>"))
        var t = plugin.proxy.getPlayer(args[0]) ?: return s.sendMessage(TextComponent(ChatColor.RED.toString() + "Couldn't find that player online"))
        val message = args.slice(IntRange(1, args.size - 1)).joinToString(" ")
        var comps = ChatFormatter(s, plugin).format(message, s.server.info.name)
        comps.put(0, JSONObject(mapOf(
                "text" to "DM",
                "color" to "blue",
                "hoverEvent" to JSONObject(mapOf(
                        "action" to "show_text",
                        "value" to ChatColor.BLUE.toString() + "This is a direct message"
                ))
        )))

        comps.put(1, JSONObject(mapOf(
                "text" to " > ",
                "color" to "gray"
        )))
        var j = comps.getJSONObject(8)
        j.put("italic", true)
        j.put("color", "dark_gray")
        comps.put(8, j)
        PlayerReplies.addReplies(t.uniqueId, s.uniqueId)
        t.sendMessage(*ComponentSerializer.parse(comps.toString(0)))
        s.sendMessage(TextComponent("${ChatColor.GOLD}[${ChatColor.RED}you ${ChatColor.GOLD}->${ChatColor.RED} ${t.displayName}${ChatColor.GOLD}]: ${ChatColor.RESET}"))
    }
}