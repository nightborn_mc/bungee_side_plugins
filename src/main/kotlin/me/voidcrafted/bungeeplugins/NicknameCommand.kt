package me.voidcrafted.bungeeplugins

import kotlinx.coroutines.experimental.launch
import me.lucko.luckperms.LuckPerms
import net.md_5.bungee.api.ChatColor
import net.md_5.bungee.api.CommandSender
import net.md_5.bungee.api.chat.TextComponent
import net.md_5.bungee.api.connection.ProxiedPlayer
import net.md_5.bungee.api.plugin.Command
import net.md_5.bungee.chat.ComponentSerializer
import org.json.JSONArray
import org.json.JSONObject

class NicknameCommand(val plugin: BungeeSidePlugins) : Command("nickname", "void.nick", "nick", "nickn", "name") {
    /**
     * Execute the command
     *
     * @param sender the executor of this command
     * @param args arguments used to invoke this command
     */
    override fun execute(sender: CommandSender?, args: Array<out String>?) {

        if (sender !is ProxiedPlayer) {
            sender?.sendMessage("You must be a player :(")
            return
        }
        val p = sender as ProxiedPlayer
        args ?: run {
            sender.sendMessage(*ComponentSerializer.parse(JSONObject(
                    mapOf(
                            "text" to "Usage: /nick <nickname>",
                            "color" to "red"
                    )).toString()))
            return
        }

        var desiredNickname = args.joinToString(" ")
        if (args.isEmpty()) {
            launch {
                val api = LuckPerms.getApi()
                val u = api.getUser(p.uniqueId) ?: return@launch
                u.clearMatching { it.isMeta && it.permission.startsWith("meta.nickname") }
                api.userManager.saveUser(u)
                sender.sendMessage(*ComponentSerializer.parse(JSONObject(
                        mapOf(
                                "text" to "Removed your nickname.",
                                "color" to "green"
                        )).toString()))
            }
            return
        }
        if (desiredNickname.length > 20) {
            sender.sendMessage(*ComponentSerializer.parse(JSONObject(
                    mapOf(
                            "text" to "The nickname must be 20 characters or less.",
                            "color" to "red"
                    )).toString()))
            return
        }

        launch {
            val api = LuckPerms.getApi()
            val u = api.getUser(p.uniqueId) ?: return@launch
            u.clearMatching { it.isMeta && it.permission.startsWith("meta.nickname") }
            u.setPermission(api.nodeFactory.makeMetaNode("nickname", desiredNickname).build())
            api.userManager.saveUser(u)
            p.sendMessage(*ComponentSerializer.parse(
                    JSONArray(arrayOf(
                            JSONObject(
                                    mapOf(
                                            "text" to "Set your nickname to ",
                                            "color" to "green"
                                    )
                            ),
                            JSONObject(
                                    mapOf(
                                            "text" to if (p.hasPermission("void.nick.color")) ChatColor.translateAlternateColorCodes('&', desiredNickname) else desiredNickname
                                    )
                            )

                    )).toString(0)
            ))
        }
    }
}