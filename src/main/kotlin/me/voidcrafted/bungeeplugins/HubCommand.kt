package me.voidcrafted.bungeeplugins

import net.md_5.bungee.api.CommandSender
import net.md_5.bungee.api.config.ServerInfo
import net.md_5.bungee.api.connection.ProxiedPlayer
import net.md_5.bungee.api.plugin.Command

class HubCommand(val plugin: BungeeSidePlugins) : Command("hub", "bungeecord.server.lobby", "lobby") {
    override fun execute(sender: CommandSender?, args: Array<out String>?) {
        (sender as ProxiedPlayer).connect(plugin.proxy.getServerInfo("lobby"))
    }
}