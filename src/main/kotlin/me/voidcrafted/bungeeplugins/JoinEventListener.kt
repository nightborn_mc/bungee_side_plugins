package me.voidcrafted.bungeeplugins
import de.myzelyam.api.vanish.BungeeVanishAPI
import net.md_5.bungee.api.plugin.Listener
import net.md_5.bungee.api.ChatColor
import net.md_5.bungee.api.chat.TextComponent
import net.md_5.bungee.api.event.PlayerDisconnectEvent
import net.md_5.bungee.api.event.PostLoginEvent
import net.md_5.bungee.event.EventHandler
import net.md_5.bungee.api.event.ServerSwitchEvent

class JoinEventListener( plugin: BungeeSidePlugins ): Listener {
    val plugin = plugin

    @EventHandler
    fun onServerChange(e: ServerSwitchEvent) {
        if (BungeeVanishAPI.isInvisible(e.player)) return
        plugin.proxy.broadcast(
                TextComponent(
                        ChatColor.LIGHT_PURPLE.toString() + e.player.displayName + " joined " + e.player.server.info.name
                )
        )
    }
    @EventHandler
    fun onLeave(e: PlayerDisconnectEvent) {
        if (BungeeVanishAPI.isInvisible(e.player)) return
        plugin.proxy.broadcast(
                TextComponent(
                        ChatColor.RED.toString() + e.player.displayName + " left"
                )
        )
    }
    @EventHandler
    fun onLogin(e: PostLoginEvent) {
        if (BungeeVanishAPI.isInvisible(e.player)) return
        plugin.proxy.broadcast(
                TextComponent(
                        ChatColor.GREEN.toString() + e.player.displayName + " joined"
                )
        )
    }
}