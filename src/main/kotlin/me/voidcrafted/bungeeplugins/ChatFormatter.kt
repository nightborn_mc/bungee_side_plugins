package me.voidcrafted.bungeeplugins

import me.lucko.luckperms.LuckPerms
import me.voidcrafted.bungeestrings.MessageGetter
import net.md_5.bungee.api.ChatColor
import net.md_5.bungee.api.connection.ProxiedPlayer
import org.json.JSONArray
import org.json.JSONObject

class ChatFormatter {
    lateinit var meta: Map<String, String>
    var plugin: BungeeSidePlugins
    var player: ProxiedPlayer
    constructor(player_: ProxiedPlayer, plugin_: BungeeSidePlugins) {
        plugin = plugin_
        player = player_
        val api = LuckPerms.getApi()
        val player_user = api.getUser(player.uniqueId) ?: return
        val contexts = api.contextManager.getApplicableContexts(player)
        val permissionData = player_user.cachedData.getMetaData(contexts)
        meta = permissionData.meta
    }

    private fun formatServer(server: String): JSONObject {
        val s = plugin.configurator.getServers()[server] ?: throw Exception("Couldn't find server")
        return JSONObject(mapOf(
                "text" to s.shortcode,
                "color" to s.color,
                "hoverEvent" to JSONObject(mapOf(
                        "action" to "show_text",
                        "value" to ChatColor.valueOf(s.color.toUpperCase()).toString() + s.description
                ))
        ))
    }
    private fun formatRank(): JSONObject {
        return JSONObject(mapOf(
                "text" to "${meta.get("friendly_name")}",
                "color" to meta.get("color").toString(),
                "hoverEvent" to JSONObject(mapOf(
                        "action" to "show_text",
                        "value" to ChatColor.valueOf(meta.get("color")!!.toUpperCase()).toString() + meta.get("description")
                ))
        ))
    }

    private fun secondPrefix(): JSONObject? {
        meta["custom_prefix"] ?: return null

        return JSONObject(mapOf(
                "text" to ChatColor.translateAlternateColorCodes('&', meta["custom_prefix"]),
                "color" to "white",
                "hoverEvent" to JSONObject(mapOf(
                        "action" to "show_text",
                        "value" to MessageGetter.ChatFormatter.customPrefixDescription
                ))
        ))
    }
    fun formatName(): JSONObject {
        var n = meta.getOrDefault("nickname", player.name)
        if (player.hasPermission("void.nick.color")) n = ChatColor.translateAlternateColorCodes('&', n)
        return JSONObject(mapOf(
                "text" to n,
                "color" to meta.get("name_color"),
                "hoverEvent" to JSONObject(mapOf(
                        "action" to "show_text",
                        "value" to
                                "${ChatColor.GOLD}${ChatColor.BOLD}${player.name}\n" +
                                "${ChatColor.RESET}${ChatColor.GOLD}" +
                                "Discord - ${PlayerDataStorage.getPlayer(player.uniqueId)?.tag}" +
                                if (player.name == "VoidCrafted")
                                    "\n${ChatColor.RESET}${ChatColor.RED}Server Owner"
                                else ""
                ))
        ))
    }
    fun format(message:String): JSONArray {
        val msg = JSONArray()
        msg.put("")
        msg.put("")
        msg.put(this.formatServer(player.server.info.name))
        msg.put(MessageGetter.ChatFormatter.seperator1)
        msg.put(this.formatRank())
        msg.put(MessageGetter.ChatFormatter.seperator1)
        val second_prefix = secondPrefix()
        if (second_prefix != null) {
            msg.put(second_prefix)
            msg.put(msg.put(MessageGetter.ChatFormatter.seperator1))
        }
        msg.put(formatName())
        msg.put(MessageGetter.ChatFormatter.seperator2)
        msg.put(JSONObject(mapOf(
                "text" to if(player.hasPermission("void.colorchat")) ChatColor.translateAlternateColorCodes('&', message) else message
        )))
        return msg
    }
}