package me.voidcrafted.bungeestrings

import net.md_5.bungee.api.ChatColor
import net.md_5.bungee.api.chat.BaseComponent
import net.md_5.bungee.chat.ComponentSerializer
import org.json.JSONArray
import org.json.JSONObject

object MessageGetter {
    object CustomPrefixCommand {
        val mustBePlayer = "You must be a player :("
        val mustHaveArgs = ComponentSerializer.parse(JSONObject(
                mapOf(
                        "text" to "Usage: /chatcolor <prefix>",
                        "color" to "red"
                )).toString())
        val prefixRemoveSuccessful = ComponentSerializer.parse(JSONObject(
                mapOf(
                        "text" to "Removed your prefix.",
                        "color" to "green"
                )).toString())
        val greaterThan16 = ComponentSerializer.parse(JSONObject(
                mapOf(
                        "text" to "The prefix, excluding color codes, must be 16 characters or less.",
                        "color" to "red"
                )).toString())
        fun prefixSetSuccessful(prefix: String): Array<BaseComponent> = ComponentSerializer.parse(
                JSONArray(arrayOf(
                        JSONObject(
                                mapOf(
                                        "text" to "Set your prefix to ",
                                        "color" to "green"
                                )
                        ),
                        JSONObject(
                                mapOf(
                                        "text" to ChatColor.translateAlternateColorCodes('&', prefix)
                                )
                        )

                )).toString(0)
        )
    }
    object ChatFormatter {
        private fun seperator(n: Int) = JSONObject(mapOf(
                "text" to " " + (">".repeat(n)) + " ",
                "color" to "gray"
        ))

        val seperator1 = seperator(1)
        val seperator2 = seperator(2)
        val customPrefixDescription = ChatColor.GREEN.toString() + "Custom Prefix! Buy yours at the donation store!"
    }
    object DiscordAuthenticator {
        fun disconnectMessage(code: String) = ComponentSerializer.parse(JSONArray(arrayOf(
                JSONObject(),
                JSONObject(mapOf(
                        "text" to "Disconnected\n",
                        "color" to "red"
                )),
                JSONObject(mapOf(
                        "text" to "You need to link your account first. ",
                        "color" to "red"
                )),
                JSONObject(mapOf(
                        "text" to "Go to #bot-commands and type ",
                        "color" to "gray"
                )),
                JSONObject(mapOf(
                        "text" to "!link $code",
                        "color" to "aqua"
                )),
                JSONObject(mapOf(
                        "text" to " then join back again!",
                        "color" to "gray"
                ))
        )).toString(0))
    }
}