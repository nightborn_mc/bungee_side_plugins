package me.voidcrafted.bungeeconfigurator

import com.google.gson.Gson
import net.md_5.bungee.api.plugin.Plugin
import java.io.File


class Configurator(plugin: Plugin) {
    var cache: ServerInfoCollector? = null
    var gson = Gson()
    
    fun getServers(): Map<String, ServerInfo> {
        cache ?: run {
            this.cache = gson.fromJson<ServerInfoCollector>(File("./void_info.json").readText(), ServerInfoCollector::class.java)
        }
        return cache!!.info
    }
}